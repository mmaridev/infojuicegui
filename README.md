# InfoJuice GUI


![docs](https://readthedocs.org/projects/infojuicegui/badge/?version=latest)
![license](https://img.shields.io/badge/code-AGPLv3-blue.svg)
![docs-license](https://img.shields.io/badge/documentation-CC%20BY--SA-lightgrey.svg)
![screenshots](https://img.shields.io/badge/screenshots-CC0-ff69b4.svg)


This project aims to give to the software "InfoJuice" a nice and useful user interface.


Docs are available at https://infojuicegui.readthedocs.io
