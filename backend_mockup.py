# Copyright (c) 2019 Marco Marinello <marco.marinello@school.rainerum.it>
# See LICENSE file
#
# This small python file is intended to serve an HTTP server
# that will be quered by the django frontend.
# Used just for develop, returns standard hardcoded jsons.

from http.server import BaseHTTPRequestHandler, HTTPServer
import json


HOSTNAME = 'localhost'
PORT = 9000


class InfoJuiceBackendMockup(BaseHTTPRequestHandler):

    a = open("mockupstrings.txt")
    MOCKUP_ANSWERS = json.loads(a.read().strip())
    a.close()

    def do_GET(self):
        if self.path in self.MOCKUP_ANSWERS:
            self.send_response(200)
            self.send_header('Content-type', 'application/json')
            self.end_headers()
            self.wfile.write(bytes(self.MOCKUP_ANSWERS[self.path], 'UTF-8'))
        else:
            self.send_response(404)
            self.send_header('Content-type', 'application/json')
            self.end_headers()
            self.wfile.write(bytes('{"status": "error", "error": "404, Not found"}', 'UTF-8'))

httpd = HTTPServer((HOSTNAME, PORT), InfoJuiceBackendMockup)
try:
    httpd.serve_forever()
except KeyboardInterrupt:
    pass
httpd.server_close()
