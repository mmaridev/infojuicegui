# Copyright (c) 2019 Marco Marinello <marco.marinello@school.rainerum.it>
# See LICENSE file
#
# Forms

from django import forms
from django.conf import settings


class NewExperimentForm(forms.Form):
    customers = forms.MultipleChoiceField(
        choices=settings.CUSTOMERS,
        widget=forms.CheckboxSelectMultiple
    )

    features = forms.MultipleChoiceField(
        choices=settings.FEATURES,
        widget=forms.CheckboxSelectMultiple
    )

    classifier = forms.ChoiceField(choices=settings.CLASSIFIERS)

    hyperparameters = forms.ChoiceField(choices=[["no", "please select classifier"]])

    validation_score = forms.ChoiceField(choices=settings.VALIDATION_SCORES)

    content_language = forms.ChoiceField(choices=settings.CONTENT_LANGUAGES)

    content_type = forms.ChoiceField(choices=settings.CONTENT_TYPES)

    content_length = forms.ChoiceField(choices=settings.CONTENT_LENGTH)
