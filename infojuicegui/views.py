# Copyright (c) 2019 Marco Marinello <marco.marinello@school.rainerum.it>
# See LICENSE file
#
# Main django views


from django.views.generic import TemplateView
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.views.generic.edit import FormView
from . import forms


class IndexView(TemplateView):
    template_name = "index.html"


class ExperimentView(TemplateView):
    template_name = "experiment.html"
    header_fields = [
        # field name, field id
        [_("Source language"), "source_language"],
        [_("Content length"), "content_length"],
        [_("Content type"), "content_type"],
        [_("Classifier"), "classifier"],
        [_("Balancing"), "balancing"],
        [_("Validation score"), "validation_score"],
        [_("Hyperparameter values"), "hyperparameter_values"],
    ]

    def get_context_data(self, pk):
        orig = super().get_context_data(pk=pk)
        orig["hdfields"] = self.header_fields
        return orig


class NewExperimentView(FormView):
    form_class = forms.NewExperimentForm
    template_name = "newexperiment.html"

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        ctx["HYPERPARAMETERS"] = settings.HYPERPARAMETERS
        return ctx
