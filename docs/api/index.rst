API
===


This part of the docs regards the `infojuiceapi` directory and django app.
The whole API is on /api of the django WSGI.


.. toctree::
   :maxdepth: 3
   :caption: Contents:

   adapters
