Adapters
========


The adapters are the parts of this software intended to connect the django
backend and the NN backend. The base adapter class is defined in
`infojuiceapi.adapter.Adapter` and consists of an object with
a function for each call that needs to be implemented.



infojuiceapi.adapter.Adapter
----------------------------


SERVER_URI
~~~~~~~~~~

.. code-block:: python

  SERVER_URI = None

sets the backend URI to None just to make sure that
you extend this class before start using the software. This variable will
be always called by the adapter's functions to get the backend adress.


def query
~~~~~~~~~

.. code-block:: python

  def query(self, path):


The `query` function is intended to be called every time you need to interact
with the backend. In the mockup is a simple http request made via `requests`
and returns the string recived from the given URI.


def list
~~~~~~~~

.. code-block:: python

  def list(self):


The `list` function is one of the API calls. It sends a query to the
backend to get the lits of experiments from the database using the
`def query`_. Then the function parses the json and sends it to the
`def to_datatables`_ function to allow Datatables to read it directly from
the stream.
The result of the query is expected to be something like this:

.. code-block:: json

  [
  {
    "name": "Test experiment",
    "id": 1234
  },
  {
    "name": "Second experiment",
    "id": 4321
  }
  ]


def experiment
~~~~~~~~~~~~~~

.. code-block:: python

  def experiment(self, pk):


The `experiment` function sends a query to the backend with the experiment ID.
The result of the query is expected to be something like this:

.. code-block:: json

  {
    "experiment_id": 1234,
    "source_language": "it_IT",
    "content_length": "ALL",
    "content_type": "ALL",
    "classifier": "SVM_LIN",
    "balancing": true,
    "validation_score": "precision",
    "hyperparameter_values": {
      "C": [
        0.001,
        0.01,
        0.1,
        1,
        10,
        100,
        1000
      ]
    },
    "features": [
      "TitleTfIdf",
      "TextTfIdf",
      "SourceBinary"
    ],
    "customers": [
      62,
      332
    ],
    "results": {
      "62": {
        "TM5": {
          "precision": 0.8875,
          "recall": 0.876,
          "f1_score": 0.878,
          "scores": [
            {
              "content_id": 1233211,
              "title": "Doc 1 TM5",
              "text_preview": "Questi sono i primi caratteri del testo ...",
              "editor_choice": 1,
              "score": 0.9,
              "timestamp": 1559644020
            },
            {
              "content_id": 1233211,
              "title": "Doc 2 TM5",
              "text_preview": "Questi sono i primi caratteri del testo ...",
              "editor_choice": 1,
              "score": 0.8,
              "timestamp": 1559471218
            },
            {
              "content_id": 1233211,
              "title": "Doc 3 TM5",
              "text_preview": "Questi sono i primi caratteri del testo ...",
              "editor_choice": 0,
              "score": 0.7,
              "timestamp": 1559471218
            },
            {
              "content_id": 1233211,
              "title": "Doc 4 TM5",
              "text_preview": "Questi sono i primi caratteri del testo ...",
              "editor_choice": 1,
              "score": 0.7,
              "timestamp": 1559471218
            },
            {
              "content_id": 1233211,
              "title": "Doc 5 TM5",
              "text_preview": "Questi sono i primi caratteri del testo ...",
              "editor_choice": 0,
              "score": 0.8,
              "timestamp": 1559384818
            }
          ]
        },
        "TM6": {
          "precision": 0.905,
          "recall": 0.888,
          "f1_score": 0.898,
          "scores": [
            {
              "content_id": 1233211,
              "title": "Doc 1 TM6",
              "text_preview": "Questi sono i primi caratteri del testo ...",
              "editor_choice": 1,
              "score": 0.98,
              "timestamp": 1556706418
            },
            {
              "content_id": 1233211,
              "title": "Doc 2 TM6",
              "text_preview": "Questi sono i primi caratteri del testo ...",
              "editor_choice": 1,
              "score": 0.98,
              "timestamp": 1556965618
            },
            {
              "content_id": 1233211,
              "title": "Doc 3 TM6",
              "text_preview": "Questi sono i primi caratteri del testo ...",
              "editor_choice": 0,
              "score": 0.98,
              "timestamp": 1556965618
            }
          ]
        }
      },
      "332": {
        "TM5": {
          "precision": 0.8875,
          "recall": 0.876,
          "f1_score": 0.878,
          "scores": [
            {
              "content_id": 1233211,
              "title": "Doc 1 TM5",
              "text_preview": "Questi sono i primi caratteri del testo ...",
              "editor_choice": 1,
              "score": 0.9,
              "timestamp": 1559644020
            },
            {
              "content_id": 1233211,
              "title": "Doc 2 TM5",
              "text_preview": "Questi sono i primi caratteri del testo ...",
              "editor_choice": 1,
              "score": 0.8,
              "timestamp": 1559471218
            },
            {
              "content_id": 1233211,
              "title": "Doc 3 TM5",
              "text_preview": "Questi sono i primi caratteri del testo ...",
              "editor_choice": 0,
              "score": 0.7,
              "timestamp": 1559471218
            },
            {
              "content_id": 1233211,
              "title": "Doc 4 TM5",
              "text_preview": "Questi sono i primi caratteri del testo ...",
              "editor_choice": 1,
              "score": 0.7,
              "timestamp": 1559471218
            },
            {
              "content_id": 1233211,
              "title": "Doc 5 TM5",
              "text_preview": "Questi sono i primi caratteri del testo ...",
              "editor_choice": 0,
              "score": 0.8,
              "timestamp": 1559384818
            }
          ]
        },
        "TM6": {
          "precision": 0.905,
          "recall": 0.888,
          "f1_score": 0.898,
          "scores": [
            {
              "content_id": 1233211,
              "title": "Doc 1 TM6",
              "text_preview": "Questi sono i primi caratteri del testo ...",
              "editor_choice": 1,
              "score": 0.98,
              "timestamp": 1556706418
            },
            {
              "content_id": 1233211,
              "title": "Doc 2 TM6",
              "text_preview": "Questi sono i primi caratteri del testo ...",
              "editor_choice": 1,
              "score": 0.98,
              "timestamp": 1556965618
            },
            {
              "content_id": 1233211,
              "title": "Doc 3 TM6",
              "text_preview": "Questi sono i primi caratteri del testo ...",
              "editor_choice": 0,
              "score": 0.6,
              "timestamp": 1556965618
            }
          ]
        }
      }
    }
  }


def document
~~~~~~~~~~~~

.. code-block:: python

  def document(self, pk):


The `document` function sends a query to the backend with the document ID to get back
the details of the document.
The result of the query is expected to be something like this:


.. code-block:: json

  {
    "date": "2018-04-19 00:00:00.000000",
    "headlines": "headline",
    "id": 20000000,
    "page": "1",
    "source_id": "300",
    "source_language": "it_IT",
    "subhead": "subhead",
    "subtitle": "subtitle",
    "text": "Gregorio Samsa, svegliandosi una mattina da sogni agitati, si trovò trasformato, nel suo letto, in un enorme insetto immondo. Riposava sulla schiena, dura come una corazza, e sollevando un poco il capo vedeva il suo ventre arcuato, bruno e diviso in tanti segmenti ricurvi, in cima a cui la coperta da letto, vicina a scivolar giù tutta, si manteneva a fatica. Le gambe, numerose e sottili da far pietà, rispetto alla sua corporatura normale, tremolavano senza tregua in un confuso luccichio dinanzi ai suoi occhi. Cosa m’è avvenuto? pensò. Non era un sogno. La sua camera, una stanzetta di giuste proporzioni, soltanto un po’ piccola, se ne stava tranquilla fra le quattro ben note pareti. Sulla tavola, un campionario disfatto di tessuti - Samsa era commesso viaggiatore e sopra, appeso alla parete, un ritratto, ritagliato da lui - non era molto - da una rivista illustrata e messo dentro una bella cornice dorata: raffigurava una donna seduta, ma ben dritta sul busto, con un berretto e un boa di pelliccia; essa levava incontro a chi guardava un pesante manicotto, in cui scompariva tutto l’avambraccio. Lo sguardo di Gregorio si rivolse allora verso la finestra, e il cielo fosco (si sentivano...",
    "title": "Titolo Kafka",
    "type": "P"
  }


def to_datatables
~~~~~~~~~~~~~~~~~

.. code-block:: python

  def to_datatables(self, stuff):


This function simply moves the `stuff` content into a dictionary with the
`data` key which is the standard required by datatables.


infojuiceapi.adapters
---------------------

Is the file where all the adapters are imported. Is used by the function
`infojuiceapi.utils.get_adapter` to obtain the configured adapter class.


infojuiceapi.mockupadapter.MockupAdapter
----------------------------------------

Is the class used to test the software. Extends the `infojuiceapi.adapter.Adapter`_
class and sets the `SERVER_URI`_ to `localhost:9000`
