.. InfoJuice GUI documentation master file, created by
   sphinx-quickstart on Mon Jun  3 15:13:48 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to InfoJuice GUI's documentation!
=========================================

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   concept
   api/index
   gui/index
