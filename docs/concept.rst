Concept
=======


This software is intended just to create a graphical interface
of a pre-existing NN project.


Standard request flow
~~~~~~~~~~~~~~~~~~~~~


As this software is intended just to dispaly data, django does not
make any calculations.

The standard processing of a request will be the following:

1. The user visits a page (e.g. the index http://infojuice.io) to get the list of the experiments.
2. Django serves a *static* HTML template as response.
3. The user's browser reads the JavaScript code and sends a reqeust to himself (still Django) at `/api/*` .
   This mechanism allows the api to be easily adapted when changing the backend.
4. Django's API calls the adapter defined in `settings.py` functions to get a result from the real backend (MockupBackend for testing)
   and sends the json (revised if necessary) back to the browser.
5. JavaScript reads the json and fills the page.

