GUI
===

The `infojuicegui` directory contains the web part. All the static
libraries, that are Bootstrap, Fontawesome and DataTables, are provided
by the Django Common Static Files app (djcsf).


.. toctree::
   :maxdepth: 3
   :caption: Contents:

   list
   experiment
