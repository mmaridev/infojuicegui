list (aka index)
================


The first page of the website consists of the list of the experiments.
It is defined via a `django.views.generic.TemplateView` which statically
answers with the `index.html` file. Once loaded by the browser, DataTables
automatically loads via ajax the list from the API.
