experiment
==========


The experiment view is always launched with the ID (`pk`) of the experiment.
Django renders the `experiment.html` template which, once loaded ny the
browser, sends an ajax request to the API. Once recived and parsed the JSON,
a `.done` function starts filling the fileds with the gathered data.


When the page is loaded
-----------------------

A request is made to the API to get the JSON dictionary from the backend.
The whole JSON is loaded and parsed, his relevant data are saved onto `document`.


When the eye button is pressed
------------------------------

The javascript function `period_details` will be called with the period name
as argument. This functions reads the data again from `document.data` and
prepares the table. Once finished, the function `compute_period` is called
to prepare precision, recall, f1 score and the plot depending on the defined
threshold.


The `period_details` function
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Logs his start to the console, then

1. writes the period name to the period label;
2. gathers from `document.data["result"][period]["scores"]` the list of
   documents to be saved to the table;
3. loops over the documents and fills an array called `for_dt` with the
   columns needed for datatables;
4. invokes datatables re-draw with new data;
5. makes the tab visibile if it's not;
6. invokes `compute_period`;


The `commonvars` function
~~~~~~~~~~~~~~~~~~~~~~~~~


The `commonvars` function is used to get from a dataset a few parameters needed
for this project. The parameters are:

* the true positive number;
* the true negative number;
* the false positive number;
* the false negative number;
* precision;
* recall;
* F1 score.


The `compute_period` function
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Is triggered to recalculate the commonvars. It invokes the commonvars function
and puts the result in the appropriate labels.


When the threshold changes
--------------------------

The event is bind to a function which recovers the last dataset from datatables
and invokes `compute_period` with that data.
