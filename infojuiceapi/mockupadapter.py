# Copyright (c) 2019 Marco Marinello <marco.marinello@school.rainerum.it>
# See LICENSE file
#
# The mockup adapter

from infojuiceapi.adapter import Adapter


class MockupAdapter(Adapter):
    SERVER_URI = "http://ijbackend:9000"
