# Copyright (c) 2019 Marco Marinello <marco.marinello@school.rainerum.it>
# See LICENSE file
#
# Some useful function

from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from django.views.generic.base import View
from django.http import HttpResponse
from django.urls import reverse_lazy
from infojuiceapi import adapters


# This function returns the adapter class configured in settings
def get_adapter():
    if not hasattr(settings, "ADAPTER"):
        raise ImproperlyConfigured("The ADAPTER is not defined in settings.py")
    adp_name = settings.ADAPTER
    if not hasattr(adapters, adp_name):
        raise ImproperlyConfigured("The ADAPTER set in settings.py does not exist")
    return getattr(adapters, adp_name)()


# This is the class to be extended for all the API views
class APIQuery(View):
    def get(self, request, *args, **kw):
        adapter = get_adapter()
        f = getattr(adapter, self.function)
        return HttpResponse(f(*args, **kw), content_type="application/json")


# This function generates a bootstrap button with a link
def gen_btn(text, dest, kw=None, type="primary"):
    btn = "<a href='%s' class='btn btn-lg btn-%s'>%s</a>" % (
        reverse_lazy(dest, kwargs=kw),
        type,
        text
    )
    return btn
