# Copyright (c) 2019 Marco Marinello <marco.marinello@school.rainerum.it>
# See LICENSE file
#
# Adapter class model

import requests
import os
import json
from infojuiceapi import utils


class Adapter(object):
    SERVER_URI = None


    def query(self, path):
        response = requests.get(os.path.join(self.SERVER_URI, path))
        return response.text


    def to_datatables(self, stuff):
        out = {"data": stuff}
        return json.dumps(out)


    def list(self):
        data = json.loads(self.query("list"))
        prep = []
        for line in data:
            prep.append([
                "#%s" % line["id"],
                line["name"],
                utils.gen_btn(
                    "<i class='fas fa-arrow-right' aria-hidden='true'></i>",
                    "experiment",
                    {"pk": line["id"]}
                )
            ])
        return self.to_datatables(prep)


    def experiment(self, pk):
        return self.query("experiment/%s" % pk)


    def document(self, pk):
        return self.query("doc/%s" % pk)
