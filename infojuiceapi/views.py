# Copyright (c) 2019 Marco Marinello <marco.marinello@school.rainerum.it>
# See LICENSE file
#
# API views

from infojuiceapi.utils import APIQuery


class ExpListView(APIQuery):
    function = "list"


class ExperimentView(APIQuery):
    function = "experiment"


class DocumentDetailsView(APIQuery):
    function = "document"
