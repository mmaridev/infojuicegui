from django.apps import AppConfig


class InfojuiceapiConfig(AppConfig):
    name = 'infojuiceapi'
